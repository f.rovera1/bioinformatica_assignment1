package main;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import entity.FASTQ;
import entity.MyEdge;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.TransitiveReduction;
import org.jgrapht.alg.shortestpath.AllDirectedPaths;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.jgrapht.graph.GraphWalk;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static main.Constants.TOO_SHORT;

class Assignment1 {


    private String path = "./files/input/ngs-reads.fq";


    private int d = -20;                     // Indel
    private int s = -20;                      // MissMatch
    private int k = 1;                      // Match
    private int minOverlapLength = 30;      // Min Overlaps length
    private int quality = 50;               // Quality
    private int minTrimmedLength = 0;       // Min trimmed length


    private ArrayList<FASTQ> readNGS;
    private DirectedWeightedMultigraph<String, MyEdge> overlapGraph;
    private ArrayList<String> assembledString;
    private boolean error = false;



    Assignment1() {
        super();
    }


     void createFile(String title, String text){
         File file = new File("./files/" + title);

         //Create the file
         try {
             if (file.createNewFile()) {
                 System.out.println("File is created!");
             } else {
                 System.out.println("File already exists.");
             }
         } catch (IOException e) {
             e.printStackTrace();
         }
         //Write Content
         FileWriter writer = null;
         try {
             writer = new FileWriter(file);
             writer.write(text);
             writer.close();
         } catch (IOException e) {
             e.printStackTrace();
         }
     }


    public String downloadTrimmedObj() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String uglyJSONString = gson.toJson(readNGS);
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(uglyJSONString);
        String prettyJsonString = gson.toJson(je);
        createFile("trimmedObj.json", prettyJsonString);

        return prettyJsonString;
    }

    public String downloadTrimmedStr() {
        ArrayList<String> trimmedReads = (ArrayList<String>) readNGS.stream().map(FASTQ::getTrimmedSeq).collect(Collectors.toList());
        String trimmed = "";
        for(String s : trimmedReads){
            trimmed += s + "\n";

        }
        createFile("trimmedStr.txt", trimmed);
        return trimmed;
    }

    public String downloadGraph() {
        String graphSummary;
        graphSummary = "\nVERTEX:\n";
        graphSummary += overlapGraph.vertexSet().toString();
        graphSummary += "\n\nEDGES:\n";
        for (MyEdge edge : overlapGraph.edgeSet())
            graphSummary += edge.toString2() + "\n";
        createFile("graphSummary.txt", graphSummary);
        return graphSummary;
    }

    public String downloadGraphGFA1() {
        String graphSummary;
        graphSummary = "H\tVN:Z:1.0\n";


        Map<String, Integer> mapId = new HashMap<String, Integer>();
        int id = 0;

        for(String s : overlapGraph.vertexSet()){
            mapId.put(s, id);
            graphSummary += "S\t" + id + "\t" + s + "\n";
            id ++;
        }

        for(MyEdge e : overlapGraph.edgeSet()){
            graphSummary += "L\t" + mapId.get(e.getSourceVertex()) + "\t+\t" + mapId.get(e.getTargetVertex()) + "\t+\t"+ e.getEdgeWeight() + "M\n";
            id ++;
        }

        createFile("graphSummary_GFA1.txt", graphSummary);
        return graphSummary;
    }

    public String downloadAssembledStr(){
        String temp = "";
        int i = 0;
        for (String str: assembledString){
            temp += "ASSEMBLED STRING N° "+ (i+1) + "\n";
            temp += str + "\n";
        }
        createFile("AssembledString.txt", temp);
        return temp;
    }



    public void start() {

        readNGS = loadReadNGS();

        trimming();

        generateOverlapGraph();
        showGraph();

        assemble();


        downloadTrimmedStr();
        downloadTrimmedObj();
        downloadGraph();
        downloadGraphGFA1();
        downloadAssembledStr();

    }


    private String semiGlobalAlignment(String a, String b) {
        int[][] dMatrix = new int[a.length() + 1][b.length() + 1];
        String[][] dMatrixPos = new String[a.length() + 1][b.length() + 1];

        for (int i = 0; i < a.length() + 1; i++) {
            dMatrix[i][0] = i * d;
            dMatrixPos[i][0] = "up";
        }

        for (int j = 0; j < b.length() + 1; j++) {
            dMatrix[0][j] = 0;
            dMatrixPos[0][j] = "end";
        }

        dMatrix[0][0] = 0;

        int value1;
        int value2;
        int value3;

        for (int i = 1; i <= a.length(); i++) {
            for (int j = 1; j <= b.length(); j++) {
                value1 = dMatrix[i - 1][j - 1] + scoringFunction(a.charAt(i - 1), b.charAt(j - 1));
                value2 = dMatrix[i - 1][j] + d;
                value3 = dMatrix[i][j - 1] + d;

                if (value1 >= value2 && value1 >= value3) {
                    dMatrix[i][j] = value1;
                    dMatrixPos[i][j] = "diag";
                } else if (value2 >= value3) {
                    dMatrix[i][j] = value2;
                    dMatrixPos[i][j] = "up";
                } else {
                    dMatrix[i][j] = value3;
                    dMatrixPos[i][j] = "sx";
                }
            }
        }


        int max = -9999;
        int maxPos = -1;
        for (int k = 0; k <= a.length(); k++) {
            if (dMatrix[k][b.length()] > max) {
                max = dMatrix[k][b.length()];
                maxPos = k;
            }
        }

        //System.out.println("max = " + max + " pos = " + maxPos);


        // ###########################################################
        //                          PRINT
        // ###########################################################
        /*

        for (int[] matrix : dMatrix) {

            for (int i : matrix) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }

        for (String[] matrix : dMatrixPos) {
            for (String i : matrix) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }

        */
        // ###########################################################
        // ###########################################################

        int i = maxPos;
        int j = b.length();
        StringBuilder a1 = new StringBuilder();
        StringBuilder b1 = new StringBuilder();

        while (!dMatrixPos[i][j].equals("end")) {
            if (i > 0 && j > 0 && dMatrixPos[i][j].equals("diag")) {
                a1.insert(0, a.charAt(i - 1));
                b1.insert(0, b.charAt(j - 1));
                i--;
                j--;
            } else if (j > 0 && dMatrixPos[i][j].equals("sx")) {
                a1.insert(0, "-");
                b1.insert(0, b.charAt(j - 1));
                j--;

            } else if (i > 0 && dMatrixPos[i][j].equals("up")) {
                a1.insert(0, a.charAt(i - 1));
                b1.insert(0, "-");
                i--;
            }
        }

        /*
        System.out.println("str1 :" + a);
        System.out.println("str2 :" + b);
        System.out.println(a1 + "\n" + b1);
        System.out.println("a1 length: " + a1.length());
        System.out.println("b1 length: " + b1.length());
        //BBBBBBBBBBBBBXX
        //             XXAAAAAAAAAA
        */
        return a1.toString();
    }

    void generateOverlapGraph() {
        ArrayList<String> trimmedReads = (ArrayList<String>) readNGS.stream().map(FASTQ::getTrimmedSeq).collect(Collectors.toList());
        overlapGraph = new DirectedWeightedMultigraph<>(MyEdge.class);
        for (String read : trimmedReads) {
            overlapGraph.addVertex(read);
        }

        for (int i = 0; i < trimmedReads.size(); i++) {
            for (int j = 0; j < trimmedReads.size(); j++) {
                if (i != j) {
                    String a = trimmedReads.get(i);
                    String b = trimmedReads.get(j);
                    String overlap = semiGlobalAlignment(a, b);
                    if (overlap.length() >= minOverlapLength) {
                        MyEdge e = overlapGraph.addEdge(b, a);
                        overlapGraph.setEdgeWeight(e, overlap.length());
                    }
                }
            }
        }
        TransitiveReduction.INSTANCE.reduce(overlapGraph);

        deleteNoEdgeVertex();
    }

    public void showGraph() {
        OverlapGraphAdapter.showGraph(overlapGraph);

    }

    private void deleteNoEdgeVertex() {
        ArrayList<String> toDelete = new ArrayList<String>();
        for (String vertex : overlapGraph.vertexSet()) {
            if (overlapGraph.edgesOf(vertex).size() == 0)
                toDelete.add(vertex);
        }
        overlapGraph.removeAllVertices(toDelete);
    }


    // Read FASTQ obj from file
    public ArrayList<FASTQ> loadReadNGS() {
        List<String> lines;
        ArrayList<FASTQ> readNGS = new ArrayList<>();
        try {
            lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
            int rowCounter = 0;
            StringBuilder singleReadNGS = new StringBuilder();

            for (String line : lines) {
                singleReadNGS.append(line).append("\n");
                if (rowCounter == 3) {
                    String[] tokens = singleReadNGS.toString().split("\n");
                    readNGS.add(new FASTQ(tokens[0],
                            tokens[1],
                            tokens[2],
                            tokens[3]));
                    rowCounter = 0;
                    singleReadNGS = new StringBuilder();
                } else {
                    rowCounter++;
                }
            }
            this.readNGS = readNGS;
            return readNGS;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private int scoringFunction(char a, char b) {
        if (a == b)
            return k;
        else if (a == '-' || b == '-')
            return d;
        else
            return s;
    }


    // ###########################################################
    //                      TRIMMING FUNCTIONS

    public void trimming() {
        ArrayList<Integer> toDelete = new ArrayList<>();
        for (int k = 0; k < readNGS.size(); k++) {
            String trimmedStr = trimming(readNGS.get(k), quality, minTrimmedLength);
            if (trimmedStr == null || trimmedStr.equals(TOO_SHORT))
                toDelete.add(k);
            else
                readNGS.get(k).setTrimmedSeq(trimmedStr);
        }

        Collections.reverse(toDelete);
        for (int integer : toDelete) {
            readNGS.remove(integer);
        }
    }

    private String trimming(FASTQ fastq, int quality, int minLength) {
        char threshold = ((char) (Math.min(93, quality) + 33));
        char[] temp = fastq.getSeqPhred().toCharArray();
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] >= threshold)
                temp[i] = '1';
            else
                temp[i] = '0';
        }
        int[] index = getLongestSeq(temp);
        String trim;
        if (index == null)
            trim = null;
        else if (index[0] >= minLength)
            trim = fastq.getSeqPrimariaRead().substring(index[1], index[0] + index[1]);
        else
            trim = TOO_SHORT;

        System.out.println(fastq.getSeqPrimariaRead() + "  --trimming-->  " + trim);
        return trim;
    }

    private int[] getLongestSeq(char[] a) {
        int[] result = new int[2];
        int maxIdx = 0, maxLen = 0, currLen = 0, currIdx = 0;

        for (int k = 0; k < a.length; k++) {
            if (a[k] == '1') {
                currLen++;
                if (currLen == 1)
                    currIdx = k;
            } else {
                if (currLen > maxLen) {
                    maxLen = currLen;
                    maxIdx = currIdx;
                }
                currLen = 0;
            }
        }

        if (maxLen > 0) {
            result[0] = maxLen;
            result[1] = maxIdx;
            //System.out.print( "Length " + maxLen) ;
            //System.out.print( ",starting index " + maxIdx );
        } else if (currLen == a.length) {
            result[0] = currLen;
            result[1] = maxIdx;
        } else
            result = null;

        return result;
    }

    //                      END TRIMMING FUNCTIONS
    // ###########################################################


    // ###########################################################
    //                      OVERLAP GRAPH GENERATION


    //                     END OVERLAP GRAPH GENERATION
    // ###########################################################

    public ArrayList<String> assemble() {
        ArrayList<String> source = new ArrayList<>(overlapGraph.vertexSet());
        ArrayList<String> skin = new ArrayList<>(overlapGraph.vertexSet());
        for (MyEdge edge : overlapGraph.edgeSet()) {
            if (source.contains(edge.getTargetVertex()))
                source.remove(edge.getTargetVertex());

            if (skin.contains(edge.getSourceVertex()))
                skin.remove(edge.getSourceVertex());

        }

        AllDirectedPaths<String, MyEdge> pathGenerator = new AllDirectedPaths<>(overlapGraph);

        List<GraphPath<String, MyEdge>> paths = new ArrayList<>();

        for (String src : source) {
            for (String skn : skin) {
                List<GraphPath<String, MyEdge>> pathsS_S = pathGenerator.getAllPaths(src, skn, true, null);
                for (GraphPath<String, MyEdge> p : pathsS_S) {
                    paths.add(p);
                }
            }
        }
        //List gr = pathGenerator.getAllPaths(source.get(0), skin.get(0), true, null);

        ArrayList<String> assembledString = new ArrayList<>();

        for (int i = 0; i < paths.size(); i++) {
            StringBuilder assembly = new StringBuilder();
            GraphWalk temp = (GraphWalk) paths.get(i);

            Object firstEdge = temp.getEdgeList().get(0);
            String sourceVertex = ((MyEdge) firstEdge).getSourceVertex();
            assembly.append(sourceVertex);
            for (Object e : temp.getEdgeList()) {
                String targetVertex = ((MyEdge) e).getTargetVertex();
                int overlap = ((MyEdge) e).getEdgeWeight();
                assembly.append(targetVertex.substring(overlap));
            }
            assembledString.add(assembly.toString());
            System.out.println("\n-----------------");
            System.out.println("ASSEMBLY STRING: ");
            System.out.println(assembly.toString());
            System.out.println("-----------------");
        }
        this.assembledString = assembledString;
        return assembledString;
    }

    private ArrayList<FASTQ> getReadNGS() {
        return readNGS;
    }

    void setParameters(String path, String k, String s, String d, String minTrimmedLength, String minOverlapLength, String quality) {
        if (!path.equals(""))
            this.path = path;
        if (!k.equals(""))
            this.k = Integer.parseInt(k);
        if (!s.equals(""))
            this.s = Integer.parseInt(s);
        if (!d.equals(""))
            this.d = Integer.parseInt(d);
        if (!minTrimmedLength.equals(""))
            this.minTrimmedLength = Integer.parseInt(minTrimmedLength);
        if (!minOverlapLength.equals(""))
            this.minOverlapLength = Integer.parseInt(minOverlapLength);
        if (!quality.equals(""))
            this.quality = Integer.parseInt(quality);
    }

    public String getPath() {
        return path;
    }

    public int getD() {
        return d;
    }

    public int getS() {
        return s;
    }

    public int getK() {
        return k;
    }

    public int getMinOverlapLength() {
        return minOverlapLength;
    }

    public int getQuality() {
        return quality;
    }

    public int getMinTrimmedLength() {
        return minTrimmedLength;
    }

    public void setError(boolean state){
        this.error=state;
    }

    public boolean getError() {
        return error;
    }

    public DirectedWeightedMultigraph<String, MyEdge> getOverlapGraph() {
        return overlapGraph;
    }
}
