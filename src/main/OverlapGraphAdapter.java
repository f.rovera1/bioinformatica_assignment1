package main;


import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.mxgraph.layout.*;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import entity.MyEdge;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import com.mxgraph.swing.mxGraphComponent;

public class OverlapGraphAdapter {

    private static void createAndShowGui(DirectedWeightedMultigraph<String, MyEdge> g) {
        JFrame frame = new JFrame("DemoGraph");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JGraphXAdapter<String, MyEdge> graphAdapter =
                new JGraphXAdapter<String, MyEdge>(g);

        mxIGraphLayout layout = new mxHierarchicalLayout(graphAdapter);
        layout.execute(graphAdapter.getDefaultParent());

        frame.add(new mxGraphComponent(graphAdapter));

        frame.pack();
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    public static void showGraph(DirectedWeightedMultigraph<String, MyEdge> g) {
        SwingUtilities.invokeLater(() -> createAndShowGui(g));
    }
}