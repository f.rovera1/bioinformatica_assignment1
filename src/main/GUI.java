package main;/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;


public class GUI extends JFrame implements ActionListener, Serializable {

    private JTextField quality;
    private JTextField missmatch;
    private JTextField indel;
    private JTextField minLength;
    private JTextField match;
    private JTextField overlapLength;
    private JTextArea log;
    private JTextField path;
    private JLabel lblMinlength;
    private JLabel lblNewLabel;
    private JButton btnTrimming;
    private JButton btnGraph;
    private JButton btnShowGraph;
    private JButton btnAssemble;
    private JButton btnSave;
    private JButton btnDelete;
    private JButton fullDemo;
    private JLabel lblOverlaplength;
    private JLabel lblD;
    private JLabel lblNewLabel_1;
    private JLabel lblQuality;
    private JScrollPane scrollPane;
    private JLabel lblLog;
    private JButton btnSfoglia;
    private JLabel lblStepByStep;
    private JLabel lblSetParameter;
    private JLabel lblDemo;
    private JLabel lblFastqFile;
    private JLabel lblAssignment;

    private Assignment1 assignment1;

    public GUI() {
        assignment1 = new Assignment1();
        initFrame();
    }

    private void initFrame() {

        setSize(615, 570);
        setTitle("Assignment 1");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.setLayout(null);

        lblQuality = new JLabel();
        lblQuality.setBounds(81, 80, 200, 15);
        lblQuality.setText("Quality:");
        lblQuality.setFont(new Font("Serif", Font.PLAIN, 14));
        panel.add(lblQuality);

        quality = new JTextField();
        quality.setBounds(131, 77, 58, 21);
        panel.add(quality);

        lblMinlength = new JLabel();
        lblMinlength.setBounds(239, 80, 200, 15);
        lblMinlength.setText("Min Length:");
        lblMinlength.setFont(new Font("Serif", Font.PLAIN, 14));
        panel.add(lblMinlength);

        missmatch = new JTextField();
        missmatch.setBounds(313, 112, 58, 21);
        panel.add(missmatch);

        lblNewLabel = new JLabel();
        lblNewLabel.setBounds(53, 112, 200, 18);
        lblNewLabel.setText("Match score:");
        lblNewLabel.setFont(new Font("Serif", Font.PLAIN, 14));
        panel.add(lblNewLabel);

        minLength = new JTextField();
        minLength.setBounds(313, 77, 58, 21);
        panel.add(minLength);

        lblNewLabel_1 = new JLabel();
        lblNewLabel_1.setBounds(212, 112, 200, 15);
        lblNewLabel_1.setText("Missmatch score:");
        lblNewLabel_1.setFont(new Font("Serif", Font.PLAIN, 14));
        panel.add(lblNewLabel_1);

        indel = new JTextField();
        indel.setBounds(483, 112, 58, 21);
        panel.add(indel);

        lblOverlaplength = new JLabel();
        lblOverlaplength.setBounds(390, 80, 200, 15);
        lblOverlaplength.setText("Overlap Length:");
        lblOverlaplength.setFont(new Font("Serif", Font.PLAIN, 14));
        panel.add(lblOverlaplength);

        match = new JTextField();
        match.setBounds(131, 112, 58, 21);
        panel.add(match);

        overlapLength = new JTextField();
        overlapLength.setBounds(483, 77, 58, 21);
        panel.add(overlapLength);

        lblD = new JLabel();
        lblD.setBounds(414, 112, 200, 15);
        lblD.setText("Indel score:");
        lblD.setFont(new Font("Serif", Font.PLAIN, 14));
        panel.add(lblD);

        btnTrimming = new JButton();
        btnTrimming.setBounds(31, 219, 120, 25);
        btnTrimming.setText("Trimming");
        btnTrimming.addActionListener(this);
        panel.add(btnTrimming);
        setContentPane(panel);

        btnGraph = new JButton();
        btnGraph.setBounds(165, 219, 130, 25);
        btnGraph.setText("Generate graph");
        btnGraph.addActionListener(this);
        panel.add(btnGraph);
        setContentPane(panel);

        btnShowGraph = new JButton();
        btnShowGraph.setBounds(310, 219, 120, 25);
        btnShowGraph.setText("Show graph");
        btnShowGraph.addActionListener(this);
        panel.add(btnShowGraph);
        setContentPane(panel);

        btnAssemble = new JButton();
        btnAssemble.setBounds(448, 219, 120, 25);
        btnAssemble.setText("Assemble");
        btnAssemble.addActionListener(this);
        panel.add(btnAssemble);
        setContentPane(panel);

        path = new JTextField();
        path.setEditable(false);
        path.setBounds(135, 41, 295, 21);
        panel.add(path);

        btnDelete = new JButton();
        btnDelete.setBounds(369, 151, 85, 25);
        btnDelete.setText("Delete");
        btnDelete.addActionListener(this);
        panel.add(btnDelete);
        setContentPane(panel);

        btnSave = new JButton();
        btnSave.setBounds(463, 151, 85, 25);
        btnSave.setText("Save");
        btnSave.addActionListener(this);
        panel.add(btnSave);
        setContentPane(panel);

        log = new JTextArea();
        scrollPane = new JScrollPane(log);
        scrollPane.setBounds(53, 367, 498, 130);
        log.setEditable(false);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(scrollPane);

        btnSfoglia = new JButton();
        btnSfoglia.setBounds(463, 39, 85, 25);
        btnSfoglia.setText("Browse");
        btnSfoglia.addActionListener(this);
        panel.add(btnSfoglia);
        setContentPane(panel);

        lblSetParameter = new JLabel();
        lblSetParameter.setBounds(254, 10, 200, 15);
        lblSetParameter.setText("SET PARAMETER");
        panel.add(lblSetParameter);

        lblStepByStep = new JLabel();
        lblStepByStep.setBounds(262, 193, 200, 15);
        lblStepByStep.setText("STEP BY STEP");
        panel.add(lblStepByStep);

        lblLog = new JLabel();
        lblLog.setBounds(286, 349, 200, 15);
        lblLog.setText("LOG");
        panel.add(lblLog);

        lblDemo = new JLabel();
        lblDemo.setBounds(268, 270, 200, 15);
        lblDemo.setText("FULL DEMO");
        panel.add(lblDemo);

        fullDemo = new JButton();
        fullDemo.setBounds(171, 296, 250, 25);
        fullDemo.setText("Start");
        fullDemo.addActionListener(this);
        panel.add(fullDemo);
        setContentPane(panel);


        lblFastqFile = new JLabel();
        lblFastqFile.setBounds(53, 41, 200, 15);
        lblFastqFile.setText("FASTQ FILE:");
        lblFastqFile.setFont(new Font("Serif", Font.PLAIN, 14));
        panel.add(lblFastqFile);


        fillParameters();
        btnShowGraph.setEnabled(false);
        btnAssemble.setEnabled(false);
        btnGraph.setEnabled(false);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        if (comando.equals("Browse")) {
            String osName = System.getProperty("os.name");
            if (osName.equals("Mac OS X")) {
                FileDialog fd = new FileDialog(this, "Choose a file", FileDialog.LOAD);
                fd.setDirectory(System.getProperty("user.dir"));
                fd.setFile("*.fq;*.fa");
                fd.setVisible(true);
                String filename = fd.getDirectory();
                path.setText(filename);
                if (filename == null) {
                    System.out.println("You cancelled the choice");
                } else {
                    System.out.println("You chose " + filename);
                }
            } else {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("FASTQ", "fq", "fa"));
                fileChooser.setAcceptAllFileFilterUsed(true);
                int returnVal = fileChooser.showOpenDialog(null);

                fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    path.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }

        } else if (comando.equals("Start")) {
            log.append("Start...\n");
            fillParameters();
            assignment1.loadReadNGS();
            if(assignment1.getError())
                log.append("Error: File not found.");
            else {
                assignment1.start();
                log.append("End.\n");

                log.append("File di output salvati correttamente.\n");
                btnShowGraph.setEnabled(true);
                btnAssemble.setEnabled(true);
                btnGraph.setEnabled(true);
            }
        } else if (comando.equals("Save")) {
            if(!path.getText().equals("")&&
                    match.getText().matches("-?\\d+(\\.\\d+)?") &&
                    missmatch.getText().matches("-?\\d+(\\.\\d+)?") &&
                    minLength.getText().matches("-?\\d+(\\.\\d+)?") &&
                    overlapLength.getText().matches("-?\\d+(\\.\\d+)?") &&
                    quality.getText().matches("-?\\d+(\\.\\d+)?") &&
                    indel.getText().matches("-?\\d+(\\.\\d+)?")
            ) {
                assignment1.setParameters(path.getText(),
                        match.getText(),
                        missmatch.getText(),
                        indel.getText(),
                        minLength.getText(),
                        overlapLength.getText(),
                        quality.getText());
                log.append("Parmetri salvati correttamente\n");
                assignment1.setError(false);
                btnTrimming.setEnabled(true);
                fullDemo.setEnabled(true);
                btnShowGraph.setEnabled(false);
                btnAssemble.setEnabled(false);
                btnGraph.setEnabled(false);

            }
            else{
                log.append("Errore nell'inserimento dei parametri\n");
                btnTrimming.setEnabled(false);
                fullDemo.setEnabled(false);
                btnShowGraph.setEnabled(false);
                btnAssemble.setEnabled(false);
                btnGraph.setEnabled(false);
                fullDemo.setEnabled(false);
            }

        } else if (comando.equals("Delete")) {
            btnTrimming.setEnabled(false);
            btnTrimming.setEnabled(false);
            fullDemo.setEnabled(false);
            btnShowGraph.setEnabled(false);
            btnAssemble.setEnabled(false);
            btnGraph.setEnabled(false);
            fullDemo.setEnabled(false);
            log.append("Parmetri azzerarti\n");
            cleanParameters();
        } else if (comando.equals("Trimming")) {
            fillParameters();
            log.append("TRIMMING...\n");

            assignment1.loadReadNGS();
                if(assignment1.getError())
                    log.setText("Error: File not found.");
                else {
                    assignment1.trimming();
                    assignment1.downloadTrimmedStr();
                    assignment1.downloadTrimmedObj();
                    log.append("TRIMMING completato verificare i file trimmedObj.json o trimmedStr.txt\n\n");
                    btnGraph.setEnabled(true);
                }
        } else if (comando.equals("Generate graph")) {
            log.append("GENERAZIONE GRAFICO...\n");
            assignment1.generateOverlapGraph();
            assignment1.downloadGraph();
            assignment1.downloadGraphGFA1();
            log.append("GENERAZIONE GRAFICO completata, per visualizzarlo premere 'showGrap'.\nVisionare i file graphSummary.txt o graphSummary_GFA1.txt per\nla struttura del grafo.\n\n");

            btnShowGraph.setEnabled(true);
            btnAssemble.setEnabled(true);
        } else if (comando.equals("Show graph")) {
            assignment1.showGraph();
        } else if (comando.equals("Assemble")) {
            log.append("ASSEMBLAGGIO...\n");
            assignment1.assemble();
            log.append("ASSEMBLAGGIO COMPLETATO. Verificare il file\nAssembledString.txt per visualizzare tutte le stringhe.\n\n");
            assignment1.downloadAssembledStr();

        }
    }

    private void fillParameters() {
        quality.setText(String.valueOf(assignment1.getQuality()));
        missmatch.setText(String.valueOf(assignment1.getS()));
        indel.setText(String.valueOf(assignment1.getD()));
        minLength.setText(String.valueOf(assignment1.getMinTrimmedLength()));
        match.setText(String.valueOf(assignment1.getK()));
        overlapLength.setText(String.valueOf(assignment1.getMinOverlapLength()));
        path.setText(String.valueOf(assignment1.getPath()));
    }

    private void cleanParameters() {
        quality.setText("");
        missmatch.setText("");
        indel.setText("");
        minLength.setText("");
        match.setText("");
        overlapLength.setText("");
        path.setText("");
        assignment1.setParameters("","","","","","","");

    }

}

