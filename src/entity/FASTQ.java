package entity;

public class FASTQ {

    private String headerRead;
    private String seqPrimariaRead;
    private String headerSeqPhred;
    private String seqPhred;
    private String trimmedSeq;

    public FASTQ(String headerRead,
                 String seqPrimariaRead,
                 String headerSeqPhred,
                 String seqPhred) {
        this.headerRead = headerRead;
        this.seqPrimariaRead = seqPrimariaRead;
        this.headerSeqPhred = headerSeqPhred;
        this.seqPhred = seqPhred;
    }

    public FASTQ() {

    }

    public String getHeaderRead() {
        return headerRead;
    }

    public void setHeaderRead(String headerRead) {
        this.headerRead = headerRead;
    }

    public String getSeqPrimariaRead() {
        return seqPrimariaRead;
    }

    public void setSeqPrimariaRead(String seqPrimariaRead) {
        this.seqPrimariaRead = seqPrimariaRead;
    }

    public String getHeaderSeqPhred() {
        return headerSeqPhred;
    }

    public void setHeaderSeqPhred(String headerSeqPhred) {
        this.headerSeqPhred = headerSeqPhred;
    }

    public String getSeqPhred() {
        return seqPhred;
    }

    public void setSeqPhred(String seqPhred) {
        this.seqPhred = seqPhred;
    }

    public String getTrimmedSeq() {
        return trimmedSeq;
    }

    public void setTrimmedSeq(String trimmedSeq) {
        this.trimmedSeq = trimmedSeq;
    }

    @Override
    public String toString() {
        return "entity.FASTQ{" +
                "headerRead='" + headerRead + '\'' +
                ", seqPrimariaRead='" + seqPrimariaRead + '\'' +
                ", headerSeqPhred='" + headerSeqPhred + '\'' +
                ", seqPhred='" + seqPhred + '\'' +
                ", trimmedSeq='" + trimmedSeq + '\'' +
                '}';
    }
}
