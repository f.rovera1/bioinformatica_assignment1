package entity;

import org.jgrapht.graph.DefaultWeightedEdge;

public class MyEdge extends DefaultWeightedEdge {
    @Override
    public String toString() {
        return String.valueOf(getWeight());
    }

    public String toString2() {
        return getSource() + " -- " + getWeight() + " -> " + getTarget();
    }


    public int getEdgeWeight(){ return (int)getWeight();}

    public String getSourceVertex(){
        return String.valueOf(getSource());
    }

    public String getTargetVertex(){
        return String.valueOf(getTarget());
    }

}